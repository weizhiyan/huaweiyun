# 树莓派获取温度上报至华为云

#### 介绍
此项目通过树莓派的ds18b20传感器获取温度，并将获取到的温度上报至华为云服务器

### 功能
-  支持从ds18b20传感器中获取温度，并通过mqtt协议上报至华为云
-  支持修改配置文件，从而连接各大云平台或私有云
-  支持sqlite3数据库对数据的基本操作
-  支持cjson打包/解析数据
- 支持zlog系统日志
- 支持命令行参数解析：指定配置文件
#### 使用教程

- 进入目录下，输入make命令  如果是第一次输入make命令，等待的时间比较长
  功能：输入make命令后，将运行所需的开源库下载解析安装，然后进行编译操作
 _

第二次输入make ，显示开源库都已经编译好，编译操作已经进行

_ 

```
  wzy@cloud-ubuntu18:~/huaweiyun/mqtt_huaweiyun$ make
make -C 3rdlib
make[1]: Entering directory '/home/wzy/huaweiyun/mqtt_huaweiyun/3rdlib'
bash build.sh
sqlite-autoconf-3290000 already compile...
mosquitto-1.5.5 already compile...
iniparser-4.1 already compile...
zlog-1.2.15 already compile...
cJSON-1.7.14 already compile...
make[1]: Leaving directory '/home/wzy/huaweiyun/mqtt_huaweiyun/3rdlib'
gcc -I /home/wzy/huaweiyun/mqtt_huaweiyun/3rdlib/install/include *.c  -o mqtt -L /home/wzy/huaweiyun/mqtt_huaweiyun/3rdlib/install/lib -lmosquitto  -lcjson -liniparser -lzlog -lsqlite3
```

- 运行过程中可能会出现：
  ` ./mqtt: error while loading shared libraries: libcjson.so.1: wrong ELF class: ELFCLASS32`
  输入命令更改环境变量： 
`export LD_LIBRARY_PATH=./3rdlib/install/lib  `       
- 输入命令： -c为指定配置文件（配置文件在/etc路径下）
 `./mqtt -c ./etc/mqtt.conf`
运行示例

```
wzy@cloud-ubuntu18:~/huaweiyun/mqtt_huaweiyun$ ./mqtt -c ./etc/mqtt.conf                   
2021-08-16 15:37:05.154651 NOTICE (mqtt_client.c:113) - zlog init successfully

2021-08-16 15:37:05.245201 NOTICE (mqtt_client.c:190) - buff: {"services":[{"service_id":"112233ww","properties":{"devID":"RPI3B-0001","time":1629099425,"temperature":27.8}}]} will be seted

2021-08-16 15:37:05.245373 NOTICE (mqtt_client.c:208) - publish successfully!

2021-08-16 15:37:08.245597 NOTICE (mqtt_client.c:217) - There is no data in the database!

2021-08-16 15:37:11.246014 NOTICE (mqtt_client.c:217) - There is no data in the database!

2021-08-16 15:37:11.246191 NOTICE (mqtt_client.c:190) - buff: {"services":[{"service_id":"112233ww","properties":{"devID":"RPI3B-0001","time":1629099431,"temperature":27.8}}]} will be seted

2021-08-16 15:37:11.246271 NOTICE (mqtt_client.c:208) - publish successfully!

^C2021-08-16 15:37:12.086598 NOTICE (mqtt_client.c:217) - There is no data in the database!
```

#### 使用说明

-  文件说明：
   - 程序树状图：
        

       


      ![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/160731_e0a880af_9526401.png "QQ截图20210816155132.png")
   - mqtt_client.c :主程序
   - mqtt_paser.c /mqtt_paser.h ：从配置文件mqtt.conf 中解析需要用到的参数
   - database.c /database.h :基于sqlite3数据库的增删查工作。主要工作为将发布失败的数据存入数据库，并在下一次发布时将数据库中的数据发送出去。
   - cjson.c /cjson.h : 把即将要发送的数据打包成cjson格式和解析从华为云接收到的数据。
   - ds18b20.c /ds18b20.h : 从存放温度的文件中获取数据并转化为正确的温度
   - makefile：存放程序编译、文件删除的命令选项
   - /etc: 存放mqtt配置文件和zlog配置文件
   - /3rdlib : build.sh: 下载编译开源库。如zlog、mosquitto等。 makefile：调用buid.sh和一些清除工作。可以被上一层的makefile调用。
  

- 命令说明：
   - make： 文件编译生成名为 mqtt 的可执行文件 。
   -  /3rdlib  make : 被上层make调养，下载编译开源库，也可以做clean工作
   - make distclean：清除工作，提交到服务之前执行该命令，会把不需要的文件删掉，如可执行文件，数据库文件等 。依赖make clean
   - ./mqtt -c : 执行命令，-c 指定mqtt配置，-h ：打印帮助信息

### 测试用例：
1.客户端数据发布至华为云

 - 客户端发布
```
021-08-17 11:21:00.638111 NOTICE (mqtt_client.c:190) - buff: {"services":[{"service_id":"112233ww","properties":{"devID":"RPI3B-0001","time":1629170460,"temperature":27.8}}]} will be seted

2021-08-17 11:21:00.638251 NOTICE (mqtt_client.c:208) - publish successfully!

```
- 华为云收到数据


![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/112224_04b74c27_9526401.png "屏幕截图.png")


 2.数据库操作
    
2.1 将数据插入数据库中

- 发布失败
```
2021-08-17 11:26:41.848298 NOTICE (mqtt_client.c:237) - buff: {"services":[{"service_id":"112233ww","properties":{"devID":" RPI3B-0001","time":1629170795,"temperature":27.8}}]} in database will be seted

2021-08-17 11:26:41.848375 ERROR  (mqtt_client.c:240) - publish failure : No such file or directory!

```
- 插入数据库：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/112817_9bc908c5_9526401.png "屏幕截图.png")

    2.2 从数据库中取出数据并发布

```

2021-08-17 11:29:06.968724 NOTICE (mqtt_client.c:223) - There have 1 row data in the database!

2021-08-17 11:29:06.968943 NOTICE (mqtt_client.c:237) - buff: {"services":[{"service_id":"112233ww","properties":{"devID":" RPI3B-0001","time":1629170795,"temperature":27.8}}]} in database will be seted

2021-08-17 11:29:06.969028 NOTICE (mqtt_client.c:247) - publish data in database successfully!
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/113013_4c371384_9526401.png "屏幕截图.png")


    2.3 删除数据


```
2021-08-17 11:29:06.969028 NOTICE (mqtt_client.c:247) - publish data in database successfully!

2021-08-17 11:29:06.978743 NOTICE (database.c:151) - delete data from database successfully!
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/113056_1ad1af73_9526401.png "屏幕截图.png")

 





