/*********************************************************************************
 *      Copyright:  (C) 2021 weizhiyan<1933283357@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  iniparser.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(07/22/2021)
 *         Author:  weizhiyan <1933283357@qq.com>
 *      ChangeLog:  1, Release initial version on "07/22/2021 08:32:15 PM"
 *                 
 ********************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <iniparser.h>
#include "zlog.h"
#include "mqtt_paser.h"

int ini_parser(mqtt_ctx_t *mqtt_ctx,char ini_path[64])
{

    dictionary      *ini = NULL;
    const char      *str;
    int             rv ;


    ini = iniparser_load(ini_path);
    if(NULL == ini)
    {
        dzlog_error("open INI_PATH failure\n");
        rv = -1;
        goto cleanup;
    }


    str = iniparser_getstring(ini, "Broker:host", NULL);
    if(NULL == str)
    {
        dzlog_error("mqtt host is null\n");
        rv = -2;
        goto cleanup; 
    }
    strncpy(mqtt_ctx->mqtt_conf.host, str, strlen(str));
    dzlog_info("host:%s\n",mqtt_ctx->mqtt_conf.host);


    str  = iniparser_getstring(ini, "Broker:port", NULL);
    if(NULL == str)
    {                         
        dzlog_error("mqtt port is null\n");
        rv = -3;                 
        goto cleanup;            
    }
    mqtt_ctx->mqtt_conf.port = atoi(str);
    dzlog_info("port : %d\n",mqtt_ctx->mqtt_conf.port);


    str = iniparser_getstring(ini, "Broker:username", NULL);
    if(NULL == str)
    {
        dzlog_error("mqtt usrname is null\n");
        rv = -4;
        goto cleanup;
    }
    strncpy(mqtt_ctx->mqtt_conf.usrname, str, strlen(str));
    dzlog_info("usrname : %s\n",mqtt_ctx->mqtt_conf.usrname);

    str = iniparser_getstring(ini, "Broker:password", NULL);
    if(NULL == str)
    {
        dzlog_error("mqtt password is null\n");
        rv = -5;
        goto cleanup;
    }
    strncpy(mqtt_ctx->mqtt_conf.password, str, strlen(str));
    dzlog_info("passwd : %s\n",mqtt_ctx->mqtt_conf.password);


     str = iniparser_getstring(ini, "Broker:client_id", NULL);
     if(NULL == str)
     {
         dzlog_error("mqtt clientID is null\n");
         rv = -6;
         goto cleanup;
     }
     strncpy(mqtt_ctx->mqtt_conf.client_id, str, strlen(str)); 
     dzlog_info("client_id : %s\n",mqtt_ctx->mqtt_conf.client_id);

     
     str = iniparser_getstring(ini, "Topic:sub_topic", NULL);
     if(NULL == str)
     {
         dzlog_error("mqtt topic is null\n");
         rv = -7;
         goto cleanup;
     }
     strncpy(mqtt_ctx->mqtt_conf.topic_sub, str, strlen(str)); 
     dzlog_info("topic : %s\n",mqtt_ctx->mqtt_conf.topic_sub);


     str = iniparser_getstring(ini, "Topic:pub_topic", NULL);
     if(NULL == str)
     {
         dzlog_error("mqtt topic_pub is null\n");
         rv = -7;
         goto cleanup;
     }
     strncpy(mqtt_ctx->mqtt_conf.topic_pub, str, strlen(str));
     dzlog_info("topic : %s\n",mqtt_ctx->mqtt_conf.topic_pub);

     
     str = iniparser_getstring(ini, "Topic:pub_qos", NULL);
     if(NULL == str)
     {
         dzlog_error("mqtt pub_qos is null\n");
         return -9;
         goto cleanup;
     }
     mqtt_ctx->mqtt_conf.pub_qos = atoi(str);
     dzlog_info("pub_qos : %d\n",mqtt_ctx->mqtt_conf.pub_qos);

     str = iniparser_getstring(ini, "Topic:sub_qos", NULL);
     if(NULL == str) 
     {
         dzlog_error("mqtt sub_qos is null\n");
         return -9; 
         goto cleanup;
     }
     mqtt_ctx->mqtt_conf.sub_qos = atoi(str);
     dzlog_info("sub_qos : %d\n",mqtt_ctx->mqtt_conf.sub_qos);

     str = iniparser_getstring(ini, "Topic:interval", NULL);
     if(NULL == str)
     {
         dzlog_error("topic interval is null\n");
         return -9;
          goto cleanup;
     }
     mqtt_ctx->mqtt_conf.interval = atoi(str);
     dzlog_info("sub_qos : %d\n",mqtt_ctx->mqtt_conf.interval);


     str = iniparser_getstring(ini, "Common:devid", NULL);
     if(NULL == str)
     {
         dzlog_error("Common devid is null\n");
         rv = -8;
         goto cleanup;
     }
     strncpy(mqtt_ctx->common_conf.devid, str, strlen(str));
     dzlog_info("topic : %s\n",mqtt_ctx->common_conf.devid);

     str = iniparser_getstring(ini, "Common:log_file", NULL);
     if(NULL == str)
     {
         dzlog_error("Common logfile is null\n");
         rv = -9;
         goto cleanup;
     }
     strncpy(mqtt_ctx->common_conf.logfile, str, strlen(str));
     dzlog_info("topic : %s\n",mqtt_ctx->common_conf.logfile);


     str = iniparser_getstring(ini, "Common:db_file", NULL);
     if(NULL == str)
     {
         dzlog_error("Common dbfile is null\n");
         rv = -10;
         goto cleanup;
     }
     strncpy(mqtt_ctx->common_conf.dbfile, str, strlen(str));
     dzlog_info("topic : %s\n",mqtt_ctx->common_conf.dbfile);

cleanup:
     iniparser_freedict(ini);
     return rv;

}

