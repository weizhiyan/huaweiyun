/*********************************************************************************
 *      Copyright:  (C) 2021 weizhiyan<1933283357@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  ds18b20.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(07/19/2021)
 *         Author:  weizhiyan <1933283357@qq.com>
 *      ChangeLog:  1, Release initial version on "07/19/2021 01:35:15 PM"
 *                 
 ********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <errno.h>


int ds18b20_get_temperature(double *temp)
{
#if 0
    /* The path to the temperature storage */
    char            w1_path[50] = "/sys/bus/w1/devices/";
    char            chip[20];
    char            buf[128];
    DIR            *dirp;
    struct dirent  *direntp;
    int             fd =-1;
    char           *ptr;
    float           value;
    int             found = 0;

    if( !temp )
    {
        printf("temp is wild pointer\n");
        return -1;
    }

    /* Open the file */
    if((dirp = opendir(w1_path)) == NULL)
    {
        printf("opendir error: %s\n", strerror(errno));
        return -2;
    }

    /* Read the temperature */
    while((direntp = readdir(dirp)) != NULL)
    {
        if(strstr(direntp->d_name,"28-"))
        {
            strcpy(chip,direntp->d_name);
            found = 1;
            break;
        }
    }
    closedir(dirp);

    if( !found )
    {
        printf("Can not find ds18b20 in %s\n", w1_path);
        return -3;
    }

    strncat(w1_path, chip, sizeof(w1_path)-strlen(w1_path));
    strncat(w1_path, "/w1_slave", sizeof(w1_path)-strlen(w1_path));

    if( (fd=open(w1_path, O_RDONLY)) < 0 ) 
    { 
        printf("open %s error: %s\n", w1_path, strerror(errno)); 
        return -4;
    } 

    if(read(fd, buf, sizeof(buf)) < 0)
    {
        printf("read %s error: %s\n", w1_path, strerror(errno)); 
        return -5;
    } 

    ptr = strstr(buf, "t=");
    if( !ptr )
    {
        printf("ERROR: Can not get temperature\n");
        return -6;
    }

    ptr+=2;

    *temp = atof(ptr)/1000;

    close(fd);

#else
    *temp = 27.8;
#endif


    return 0;
}
