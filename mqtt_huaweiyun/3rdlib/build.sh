#!/bin/bash


PRJ_PATH=`pwd`

DL_ADDR_mos=http://mosquitto.org/files/source/
DL_ADDR_PASER=https://codeload.github.com/ndevilla/iniparser/tar.gz/refs/tags/v4.1
DL_ADDR_ZLOG=https://codeload.github.com/HardySimpson/zlog/tar.gz/refs/tags/1.2.15
DL_ADDR_SQL=ftp://master.iot-yun.club/src/
DL_ADDR_CJSON=ftp://master.iot-yun.club/src/


function compile_mosquitto()
{
    SRC_NAME=mosquitto-1.5.5

    if [ -f ${PRJ_PATH}/install/lib/libmosquitto.so ] ; then
        echo "${SRC_NAME} already compile..."
        return ;
    fi


    if [ ! -f ${SRC_NAME}.tar.gz ] ; then
    wget ${DL_ADDR_mos}/${SRC_NAME}.tar.gz
    fi

    if [ ! -d ${SRC_NAME} ] ; then
    tar -xzvf ${SRC_NAME}.tar.gz
    fi

    cd ${SRC_NAME}
    make && make install prefix=${PRJ_PATH}/install
    cd -
}


function compile_inipaser()
{
    SRC_NAME=iniparser-4.1

    if [ -f ${PRJ_PATH}/install/lib/libiniparser.a ] ; then
        echo "${SRC_NAME} already compile..."
        return ;
    fi
    if [ ! -f ${SRC_NAME}.tar.gz ] ; then
        wget ${DL_ADDR_PASER} -O ${SRC_NAME}.tar.gz
    fi

    if [ ! -d ${SRC_NAME} ] ; then
        tar -xzvf ${SRC_NAME}.tar.gz
    fi

    cd ${SRC_NAME}
    make 

    cp libiniparser.*  ${PRJ_PATH}/install/lib
    cp src/*.h ${PRJ_PATH}/install/include
    cd -
}

function compile_zlog()
{
    SRC_NAME=zlog-1.2.15

    if [ -f ${PRJ_PATH}/install/lib/libzlog.so ] ; then
        echo "${SRC_NAME} already compile..."
        return ;
    fi

    if [ ! -f ${SRC_NAME}.tar.gz ] ; then
    wget https://codeload.github.com/HardySimpson/zlog/tar.gz/refs/tags/1.2.15 -O zlog-1.2.15.tar.gz

    fi 
    if [ ! -d ${SRC_NAME} ] ; then
        tar -xzvf ${SRC_NAME}.tar.gz
    fi
    cd ${SRC_NAME}
    make

    cp src/libzlog.*  ${PRJ_PATH}/install/lib
    cp src/zlog.h ${PRJ_PATH}/install/include
    cd -
}

function compile_sqlite()
{
    SRC_NAME=sqlite-autoconf-3290000

    if [ -f ${PRJ_PATH}/install/lib/libsqlite3.so ] ; then
        echo "${SRC_NAME} already compile..."
        return ;
    fi

    if [ ! -f ${SRC_NAME}.tar.gz ] ; then
        wget ${DL_ADDR_SQL}/${SRC_NAME}.tar.gz
    fi

    if [ ! -d ${SRC_NAME} ] ; then
        tar -xzf ${SRC_NAME}.tar.gz
    fi

    cd ${SRC_NAME}
    ./configure --prefix=${PRJ_PATH}/install
    make && make install
    cd -
}

function compile_cjson()
{
    SRC_NAME=cJSON-1.7.14

    if [ -f ${PRJ_PATH}/install/lib/libcjson.so ] ; then
        echo "${SRC_NAME} already compile..."
        return ;
    fi

    if [ ! -f ${SRC_NAME}.tar.gz ] ; then
        wget ${DL_ADDR_CJSON}/${SRC_NAME}.tar.gz
    fi

    if [ ! -d ${SRC_NAME} ] ; then
        tar -xzf ${SRC_NAME}.tar.gz
    fi

    cd ${SRC_NAME}
    make && make install PREFIX=${PRJ_PATH}/install
    export LD_LIBRARY_PATH=./install/lib/

    cd -
}


if [[ $1 = "clean" ]] ; then
    rm -rf cJSON-1.7.14
    rm -rf sqlite-autoconf-3290000
    rm -rf zlog-1.2.15
    rm -rf iniparser-4.1
    rm -rf mosquitto-1.5.5
    exit ;
elif [[ $1 = "distclean" ]] ; then
    rm -rf cJSON-1.7.14.*
    rm -rf sqlite-autoconf-3290000.*
    rm -rf zlog-1.2.15.*
    rm -rf iniparser-4.1.*
    rm -rf mosquitto-1.5.5.*

    rm -rf install
    exit ;
fi

 compile_sqlite
 compile_mosquitto
 compile_inipaser
 compile_zlog
 compile_cjson
