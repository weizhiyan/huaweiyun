/********************************************************************************
 *      Copyright:  (C) 2021 weizhiyan<1933283357@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  database.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(08/02/2021)
 *         Author:  weizhiyan <1933283357@qq.com>
 *      ChangeLog:  1, Release initial version on "08/02/2021 06:34:37 PM"
 *                 
 ********************************************************************************/

#ifndef _OPEN_DATABASE_H
#define _OPEN_DATABASE_H

extern int open_database(sqlite3 **db, char dbfile[64]);
extern int insert_data(sqlite3 *db,time_t tm,char *devid,double temp);
extern int get_table(sqlite3 *db);
extern int get_data(sqlite3 *db,char *devid,int *tm,double *temp,int *rowid,int size_devid);
extern int delete_data(sqlite3 *db,int rowid); 

#endif

