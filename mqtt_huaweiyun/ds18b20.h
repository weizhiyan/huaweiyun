/********************************************************************************
 *      Copyright:  (C) 2021 weizhiyan<1933283357@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  ds18b20.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(07/19/2021)
 *         Author:  weizhiyan <1933283357@qq.com>
 *      ChangeLog:  1, Release initial version on "07/19/2021 01:37:18 PM"
 *                 
 ********************************************************************************/


#ifndef _OPEN_DS18B20_H
#define _OPEN_DS18B20_H

extern int ds18b20_get_temperature(double *temp);

#endif

