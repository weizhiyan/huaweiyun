/********************************************************************************
 *      Copyright:  (C) 2021 weizhiyan<1933283357@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  iniparser.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(07/22/2021)
 *         Author:  weizhiyan <1933283357@qq.com>
 *      ChangeLog:  1, Release initial version on "07/22/2021 08:43:21 PM"
 *                 
 ********************************************************************************/

#ifndef  _OPEN_INIPARSER_H
#define _OPEN_INIPARSER_H


typedef struct conf_s
{
    char devid[16];
    char logfile[64];
    char dbfile[64];

}conf_t;


typedef struct mqtt_s
{
    char    host[64];
    char    usrname[128];
    char    password[128];
    char    client_id[128];
    char    topic_sub[128];
    char    topic_pub[128];
    int     sub_qos;
    int     pub_qos;
    int     interval;
    int     port;

}mqtt_t;

typedef struct mqtt_ctx_s
{
    conf_t common_conf;
    mqtt_t mqtt_conf;

}mqtt_ctx_t;

extern int ini_parser(mqtt_ctx_t *mqtt_ctx,char ini_path[64]);


#endif


