/********************************************************************************
 *      Copyright:  (C) 2021 weizhiyan<1933283357@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  cjson.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(07/19/2021)
 *         Author:  weizhiyan <1933283357@qq.com>
 *      ChangeLog:  1, Release initial version on "07/19/2021 03:18:19 PM"
 *                 
 ********************************************************************************/


#ifndef _OPEN_JSON_H
#define _OPEN_JSON_H

extern int Parse_json(char *buf, char *id, double *tem,int size_id,time_t time);
extern int pack_json(char *buf,double temp,char *devid,time_t tm);

#endif

